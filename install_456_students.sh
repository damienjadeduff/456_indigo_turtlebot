#!/bin/bash

echo 
echo WARNING - Please do NOT run as superuser - some permissions may get skewed if you do
echo 
echo BUT - Parts of this script may request superuser password.
echo 
echo WARNING - Parts of this script may be interactive - I put these bits at the end.
echo 
echo WARNING - This script causes ubuntu to upgrade.
echo 
echo WARNING - If this script does not end with \"ROS Indigo install with extra packages was successful.\", it means that the installation did not finish.
echo
echo NOTE: To make this script install extra cool robots - Hector, Nao, Husky - uncomment out lines 51 -- 57 and rerun it
echo 
echo Press enter to proceed
read temp

echo Adding ROS repos...
# add ros
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu trusty main" > /etc/apt/sources.list.d/ros-latest.list'

echo Getting key for ROS....
# add ros keys
wget --ca-certificate=/etc/ssl/certs/DigiCert_High_Assurance_EV_Root_CA.pem https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -O - | sudo apt-key add - # for some reason wget does not pick the right certificate on some installations - not on mine! On mine it is very very smart.
wget https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -O - | sudo apt-key add - 

echo Asking APT to download the latest source list....
# update APT
sudo apt-get -y update || exit 1

echo Asking APT to upgrade what it has not yet upgraded
# upgrade what is there
sudo apt-get -y upgrade
sudo apt-get -y dist-upgrade

echo Now installing all the extra packages that we want - including ROS

#for building, version control and starting IDE (kdevelop)
sudo apt-get -y install build-essential clang git gitk git-cola mercurial tortoisehg python-rosinstall dos2unix python-wstool kdevelop kate || exit 1

#core BLG456E (assignments)
sudo apt-get -y install ros-indigo-turtlebot-gazebo ros-indigo-desktop-full ros-indigo-turtlebot-rviz-launchers  ros-indigo-warehouse-ros  || exit 1

sudo apt-get -y install  ros-indigo-gazebo-ros ros-indigo-gazebo-plugins ros-indigo-gazebo-ros-control gazebo2-dbg python-rosdep python-wstool ros-indigo-ros ros-indigo-warehouse-ros libspnav-dev || exit 1

sudo apt-get -y install ros-indigo-depthimage-to-laserscan  ros-indigo-slam-gmapping ros-indigo-map-server ros-indigo-costmap-2d ros-indigo-move-base ros-indigo-amcl ros-indigo-navigation ros-indigo-trajectory-msgs 

sudo apt-get -y install ros-indigo-pcl-ros ros-indigo-perception-pcl || exit 1

# extra BLG456E robot simulations
# sudo apt-get -y install ros-indigo-katana-description ros-indigo-katana-arm-gazebo ros-indigo-katana-tutorials || exit 1
# 
# sudo apt-get -y install ros-indigo-hector-gazebo-worlds ros-indigo-hector-gazebo-plugins ros-indigo-hector-gazebo ros-indigo-hector-slam ros-indigo-hector-localization ros-indigo-hector-mapping || exit 1
# 
# sudo apt-get -y install ros-indigo-husky-simulator ros-indigo-husky-navigation ros-indigo-husky-viz ros-indigo-husky-control || exit 1
# 
# sudo apt-get -y install ros-indigo-nao-meshes ros-indigo-nao-teleop ros-indigo-nao-gazebo-plugin || exit 1 #accept agreement for Nao meshes

echo Doing some ROS cleanup

sudo rosdep init
rosdep update || exit 1

echo "----------------------------------------------"
echo "----------------------------------------------"
echo ROS Indigo install with extra packages finished.
echo "----------------------------------------------"
echo "----------------------------------------------"

exit 0
