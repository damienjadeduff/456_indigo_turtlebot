#!/bin/bash

echo 
echo WARNING - Please do NOT run as superuser - some permissions may get skewed if you do
echo 
echo BUT - Parts of this script may request superuser password.
echo 
echo WARNING - Parts of this script may be interactive - I put these bits at the end.
echo 
echo WARNING - This script causes ubuntu to upgrade.
echo 
echo WARNING - If this script does not end with \"ROS Indigo install with extra packages was successful.\", it means that the installation did not finish.
echo 
echo Press enter to proceed
read temp

echo Adding ROS repos...
# add ros
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu trusty main" > /etc/apt/sources.list.d/ros-latest.list'

echo Getting key for ROS....
# add ros keys
wget --ca-certificate=/etc/ssl/certs/DigiCert_High_Assurance_EV_Root_CA.pem https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -O - | sudo apt-key add - # for some reason wget does not pick the right certificate on some installations - not on mine! On mine it is very very smart.
wget https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -O - | sudo apt-key add - 
# wget https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -O - | sudo apt-key add - || exit 1

echo Asking APT to download the latest source list....
# update APT
sudo apt-get -y update || exit 1

echo Asking APT to upgrade what it has not yet upgraded
# upgrade what is there
sudo apt-get -y upgrade
sudo apt-get -y dist-upgrade

echo Now installing all the extra packages that we want - including ROS

sudo apt-get -y install build-essential clang git gitk git-cola mercurial tortoisehg python-rosinstall dos2unix python-wstool geany kdevelop kate ros-indigo-warehouse-ros libspnav-dev || exit 1

#core BLG456E (assignments)
sudo apt-get -y install ros-indigo-turtlebot-gazebo ros-indigo-desktop-full ros-indigo-turtlebot-rviz-launchers  || exit 1

sudo apt-get -y install  ros-indigo-gazebo-ros ros-indigo-gazebo-plugins ros-indigo-gazebo-ros-control gazebo2-dbg python-rosdep python-wstool ros-indigo-ros ros-indigo-warehouse-ros libspnav-dev || exit 1

#utils
sudo apt-get -y install elinks python3-scipy python3 synaptic python-numpy python-scipy python python3-numpy blender inkscape gimp gimp-help-en gimp-plugin-registry python-pygame python-pyglet libreoffice libreoffice-help-tr texlive-latex-base geany geany-plugin-latex geany-plugin-spellcheck kate kdevelop jabref texlive-lang-european latexdiff pdftk imagemagick okteta texlive-bibtex-extra texlive-publishers python-sklearn  python-skimage python3-skimage python-pil python3-pil python-sympy python-simpy python-simpy-gui python3-simpy  libopencv-dev opencv-doc wine q4wine   ros-indigo-depthimage-to-laserscan  python-ipdb python3-ipdb ipython3 ipython-qtconsole ipython-notebook ipython3-notebook ipython3-qtconsole python-opencv ros-indigo-slam-gmapping ros-indigo-map-server ros-indigo-costmap-2d ros-indigo-move-base ros-indigo-amcl ros-indigo-navigation ros-indigo-trajectory-msgs kdev-python octave-ga octave octave-symbolic wxmaxima octave-plot octave-doc vlc libav-tools kdenlive audacity recordmydesktop istanbul  python-bottle python3-bottle python-bottle-doc python-flask python3-flask python-flask-doc

# extra BLG456E robot simulations
sudo apt-get -y install ros-indigo-katana-arm-gazebo ros-indigo-katana-tutorials ros-indigo-katana-description ros-indigo-hector-gazebo-worlds ros-indigo-hector-gazebo-plugins ros-indigo-hector-gazebo ros-indigo-hector-slam ros-indigo-hector-localization ros-indigo-hector-mapping ros-indigo-husky-simulator ros-indigo-husky-navigation ros-indigo-husky-viz ros-indigo-husky-control || exit 1

sudo apt-get -y install ros-indigo-nao-meshes ros-indigo-nao-teleop ros-indigo-nao-gazebo-plugin || exit 1 #accept agreement for Nao meshes

# for project students:
sudo apt-get install ros-indigo-pcl-ros ros-indigo-perception-pcl ros-indigo-ecto-pcl ros-indigo-openni-launch openni-utils ros-indigo-openni2-launch openni2-utils  libqt4-opengl  libqtcore4 libqtgui4 || exit 1
cd /tmp
wget files.djduff.net/iisu/IISUy/DepthSenseSDK-1.4.5-2151-amd64-deb.run
chmod u+x DepthSenseSDK-1.4.5-2151-amd64-deb.run
sudo ./DepthSenseSDK-1.4.5-2151-amd64-deb.run || exit 1 # accept agreement

echo Doing some ROS cleanup

sudo rosdep init
rosdep update || exit 1

echo "----------------------------------------------"
echo "----------------------------------------------"
echo ROS Indigo install with extra packages finished.
echo "----------------------------------------------"
echo "----------------------------------------------"

exit 0
